<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
        crossorigin="anonymous">
    <style>
        body{
            margin-top: 2%;
            margin-bottom: 2%;
        }

        .json-form{
            margin-top: 1%;
            margin-bottom: 1%;
        }
        button[type='submit']{
            margin-top: 1%;
            margin-bottom: 1%;
        }
        .form-col{
            padding: 1%;
        }


        </style>
    <title>Spine JSON Comparer</title>
</head>

<body class="container">
    <form class="row json-form" action="" method="post" enctype="multipart/form-data">
        <div class="col-xl-6 border border-primary rounded form-col">
            <div class="form-group">
                <label for="stdFile">Select standard file to upload:</label>
                <input type="file" name="stdFile" id="stdFile">
                <small class="form-text text-muted">JSON files only</small>
            </div>
            <div class="form-group">
                <label for="stdChar">Character name from standard file:</label>
                <input type="text" name="stdChar" id="stdChar">
                <small class="form-text text-muted">Name of character to ignore while compare</small>
            </div>
        </div>
        <div class="col-xl-6 border border-primary rounded form-col">
            <div class="form-group">
                <label for="cmpFile"> Select file to compare:</label>
                <input type="file" name="cmpFile" id="cmpFile">
                <small class="form-text text-muted">JSON files only</small>
            </div>
            <div class="form-group">
                <label for="cmpChar">Character name from file to compare:</label>
                <input type="text" name="cmpChar" id="cmpChar">
                <small class="form-text text-muted">Name of character to ignore while compare</small>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Compare Json animations</button>
    </form>

    <?php
$target_dir = "uploads/";
$date = new DateTime();
$stdAnimations = [];
$cmpAnimations = [];
$stdOk = false;
$cmpOk = false;

//standard file:
if(isset($_FILES["cmpFile"])){
    $stdFile = $target_dir . ($date->getTimestamp()) . basename($_FILES["stdFile"]["name"]);
    $stdUploadOk = 1;
    $stdFileType = strtolower(pathinfo($stdFile,PATHINFO_EXTENSION));
    if($stdFileType == "json"){
        if (move_uploaded_file($_FILES["stdFile"]["tmp_name"], $stdFile)) {   
            $json = file_get_contents($stdFile);
            $JSONdata = json_decode($json, true);
            $JSONdataValue = $JSONdata['animations'];
            foreach ($JSONdataValue as $key => $val) {
                $stdAnimations[] = $key;
            }
            $stdOk = true;
            unlink($stdFile);
        }
         else {
            echo "<div class='alert alert-danger' role='alert'>Standard Json: there was an error uploading your file.</div>";
        
        }   
    }
    else{
        echo "<div class='alert alert-danger' role='alert'>Standard Json: Error - only JSON files can be analyzed.</div>";
    } 
}


//file to compare
if(isset($_FILES["cmpFile"])){
    $cmpFile = $target_dir . ($date->getTimestamp()) . basename($_FILES["cmpFile"]["name"]);
    $cmpUploadOk = 1;
    $cmpFileType = strtolower(pathinfo($cmpFile,PATHINFO_EXTENSION));
    if($cmpFileType == "json"){
        if (move_uploaded_file($_FILES["cmpFile"]["tmp_name"], $cmpFile)) {   
            $json = file_get_contents($cmpFile);
            $JSONdata = json_decode($json, true);
            $JSONdataValue = $JSONdata['animations'];
            foreach ($JSONdataValue as $key => $val) {
                $cmpAnimations[] = $key;
            }
            $cmpOk = true;
            unlink($cmpFile);
        }
         else {
            echo "<div class='alert alert-danger' role='alert'>Json to compare: there was an error uploading your file.</div>";
        }   
    }
    else{
        echo "<div class='alert alert-danger' role='alert'>Json to compare: Error - only JSON files can be analyzed.</div>";
    } 
}


//compare Jsons:
if($cmpOk == true && $stdOk == true){
    $stdCharName = $_POST["stdChar"];
    $cmpCharName = $_POST["cmpChar"];

    echo "<h2>Animations in standard file (" . basename($_FILES["stdFile"]["name"]) . "):</h2>";
    echo '<ul class="list-group">';
    foreach($stdAnimations as $key => $val){
        echo '<li class="list-group-item">' . $val . '</li>';
    }
    echo '</ul>';

    echo "<h2>Animations in file to compare (" . basename($_FILES["cmpFile"]["name"]) . "):</h2>";
    echo '<ul class="list-group">';
    foreach($cmpAnimations as $key => $val){
        echo '<li class="list-group-item">' . $val . '</li>';
    }
    echo '</ul>';

    //ignoring characters names:
    if($stdCharName != null)
    {
        $stdAnimations = str_replace($stdCharName,"{Name}",$stdAnimations);
    }

    if($cmpCharName != null)
    {
        $cmpAnimations = str_replace($cmpCharName,"{Name}",$cmpAnimations);
    }

    $missingAnimations = array_diff($stdAnimations, $cmpAnimations);
    echo "<h2>Animations missing:</h2>";
    echo '<ul class="list-group">';
    foreach($missingAnimations as $key => $val){
        echo '<li class="list-group-item">' . $val . '</li>';
    }
    echo '</ul>';

    $unnecessaryAnimations = array_diff($cmpAnimations, $stdAnimations);
    echo "<h2>Unnecessary animations:</h2>";
    echo '<ul class="list-group">';
    foreach($unnecessaryAnimations as $key => $val){
        echo '<li class="list-group-item">' . $val . '</li>';
    }
    echo '</ul>';
}
?>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
</body>

</html>